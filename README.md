oddsportal user navigator
=========================

## About

This is a simple [userscript](https://github.com/OpenUserJs/OpenUserJS.org/wiki/Userscript-beginners-HOWTO) for [Greasemonkey](https://addons.mozilla.org/firefox/addon/greasemonkey/) or similar extension, which -- upon visiting a page on [oddsportal](https:/www.oddsportal.com) -- will allow you to switch to some predefined users and then cycle between them by pressing `x` (and to go back by pressing `\``).
