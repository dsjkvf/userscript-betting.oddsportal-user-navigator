// ==UserScript==
// @name        betting.oddsportal - users
// @namespace   oyvey
// @homepage    https://bitbucket.org/dsjkvf/userscript-betting.oddsportal-user-navigator/
// @downloadURL https://bitbucket.org/dsjkvf/userscript-betting.oddsportal-user-navigator/raw/master/o.user.js
// @updateURL   https://bitbucket.org/dsjkvf/userscript-betting.oddsportal-user-navigator/raw/master/o.user.js
// @require     http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js
// @require     https://gist.github.com/raw/2625891/waitForKeyElements.js
// @version     1.03
// @match       https://www.oddsportal.com/*
// @run-at      document-start
// @grant       none
// ==/UserScript==


// INIT

var names = ['skejg', 'strictlyunders', 'samij93', 'sven555', 'zlatan88', 'blink18222', 'HenryChinaski', 'Microuses'];
var locat = window.location.href;


// HELPERS

function getUsername(url) {
    return url.substr(url.lastIndexOf('/', url.lastIndexOf('/') - 1)).replace(/\//g, '');
}

// MAIN

// Add a hotkey to switch between usernames
addEventListener('keyup', function(e) {
    if ( e.ctrlKey ||
         e.altKey ||
         e.metaKey ||
         e.shiftKey ||
         (e.which !== 88 /*x*/ && e.which !== 192 /*`*/)) {
         return;
    };

    e.preventDefault();

    if (e.which == 88) {
        var curr = getUsername(locat);
        var next = names[names.indexOf(curr) + 1];
        if (typeof next == "undefined"){
            next = names[0];
        };
        if (window.location.href.indexOf('profile') == -1) {
            window.location.replace('https://www.oddsportal.com/profile/' + next);
        } else {
            window.location.href = locat.replace(curr, next);
        };
    } else if (e.which == 192) {
        window.history.go(-1);
    };
})
